import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'


// Vuetify
//import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import './plugins/vuetify'

const vuetify = createVuetify({
  components,
  directives,
})

createApp(App).use(vuetify).mount('#app')

//import router from './router'

//import './assets/main.css'

/*const app = createApp(App)

app.use(createPinia())
//app.use(router)

app.use(vuetify))


app.mount('#app')
*/